const express = require('express');
const axios = require("axios");

const {connectDb} = require('./helpers/db');
const {PORT, HOST, DB, API_URL} = require('./configuration');
const app = express();

const startServer = () => {
    app.listen(PORT, () => {
        console.log(`api started port: ${PORT}`);
        console.log(`api started host: ${HOST}`);
        console.log(`api started db: ${DB}`);
    });
};

app.get('/test/', (req,res) => {
    res.send('Test send auth')
});

app.get("/api/currentUser", (req, res) => {
    res.json({
        id: "1234",
        email: "foo@gmail.com"
    });
});

app.get("/testwithapidata", (req, res) => {
    axios.get(API_URL + "/testapidata").then(response => {
        res.json({
            testapidata: response.data.testapidata
        });
    });
});


connectDb()
    .on('error', console.log)
    .on('disconnected', connectDb)
    .once('open', startServer);
