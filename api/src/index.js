const express = require('express');
const mongoose = require('mongoose');
const axios = require("axios");

const {connectDb} = require('./helpers/db');
const {PORT, HOST, DB, AUTH_API_URL} = require('./configuration');
const app = express();

const postSchema = new mongoose.Schema({
    name: String
});
const Post = mongoose.model('Post', postSchema);

app.get("/testapidata", (req, res) => {
    res.json({
        testapidata: true
    });
});

app.get("/testwithcurrentuser", (req, res) => {
    axios.get(AUTH_API_URL + "/currentUser").then(response => {
        res.json({
            testwithcurrentuser: true,
            currentUserFromAuth: response.data
        });
    });
});


const startServer = () => {
    app.listen(PORT, () => {
        console.log(`api started port: ${PORT}`);
        console.log(`api started host: ${HOST}`);
        console.log(`api started db: ${DB}`);

        // Post.find(function (err, posts) {
        //     if (err) return console.error(err);
        //     console.log(posts);
        // });

        const silence = new Post({ name: 'Silence' });
        console.log(silence.name); // 'Silence'

        silence.save(function (err, savedSilence) {
            if (err) return console.error(err);
            console.log('post saveed!', savedSilence);
        });



    });
};

app.get('/test/', (req,res) => {
    res.send('Test send')
});

connectDb()
    .on('error', console.log)
    .on('disconnected', connectDb)
    .once('open', startServer);
