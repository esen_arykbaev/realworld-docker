import React from 'react';
import axios from 'axios';
import logo from './logo.svg';
import './App.css';

function App() {
  const makeFetch = () => {
    axios('/api/testwithcurrentuser').then(res => {
      console.log(res.data)
    })
  };
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React Chenge 1
        </a>
      </header>
      <button onClick={makeFetch}>click </button>
    </div>
  );
}

export default App;
